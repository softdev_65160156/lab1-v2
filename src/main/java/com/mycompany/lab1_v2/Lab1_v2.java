/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1_v2;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class Lab1_v2 {
    static char[][] table = { { '-', '-', '-' },
                            { '-', '-', '-' },
                            { '-', '-', '-' } };
    static char player = 'X';
    static int row, col;
    static int countTurn;
    
    
    public static void printWelcome(){
        System.out.println("Welcome to XO RowCol Games!");
        
    }
    
    public static void showTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println(); 
        }    
    }
    
    public static void printTurn(){
        System.out.println("Turn"+" "+player);
    }
    
    public static void inputRowCol(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Input row col:"); 
        row = sc.nextInt();
        col = sc.nextInt();
        if (checkAlready(row, col)) {
            System.out.println("This point has already been chosen by the player");
            showTable();
            System.out.println("Please input RowCol again");
            inputRowCol();
        }
        table[row][col] = player;
    }
    static boolean checkAlready(int Row, int Col) {
        if (table[Row][Col] == 'X' || table[Row][Col] == 'O')
            return true;
        return false;
    }
    public static void switchPlayer() {
        if (player == 'X')
            player = 'O';
        else
            player = 'X';
    }
   static boolean checkWinRow() {
        for (int c = 0; c < 3; c++) {
            if (table[row][c] != player) {
                return false;
            }
        }
        return true;
    }

    static boolean checkWinCol() {
        for (int r = 0; r < 3; r++) {
            if (table[r][col] != player) {
                return false;
            }
        }
        return true;
    }
    static boolean checkWinXR() {
        if (table[0][0] == player && table[1][1] == player && table[2][2] == player)
            return true;
        return false;
    }

    static boolean checkWinXL() {
        if (table[0][2] == player && table[1][1] == player && table[2][0] == player)
            return true;
        return false;
    }
    static boolean checkWin() {
        if (checkWinCol() || checkWinRow() || checkWinXR() || checkWinXL())
            return true;
        return false;
    }
    public static void showWin(){
        System.out.println("Player:"+" "+player+" "+"Win!!!");
    }

    public static void printDraw(){
        System.out.println("Player Draw!");
    }
    
    static boolean checkDraw(){
        if(countTurn == 9)
            return true;
        return false;
    }
    public static void main(String[] args) {
        printWelcome();
        showTable();
        
        for (countTurn = 1; countTurn < 10; countTurn++){
            printTurn();
            inputRowCol();
            showTable();
            if(checkWin()){
                showWin();
                break;
                
            }
        if (checkDraw()) {
            showTable();
            printDraw();
        }
        
            switchPlayer();
        }
        
        
    }

    
}
